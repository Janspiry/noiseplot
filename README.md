### Noise Curve Plot 

本文件用于可视化亮度(Y)－降噪强度($\lambda$ )曲线图，并提供拖拽/按键修改功能．

#### 功能说明：

##### 图片

读入图片，导航栏右上角会显示当前鼠标位置的Ｙ及降噪强度，双击可固定选择

##### 曲线

- 读取曲线，默认每个Y对应一个系数，共1024个数据点．可选择等距读取部分点，点太多了不好调节曲线
- 修改曲线，鼠标进行拖拽，按键I 在直线上插入新点，D进行删除，H隐藏直线



![show](image/show.gif)



#### 环境要求

```bash
pip install numpy scipy matplotlib h5py PIL argparse
```



#### 使用方法

```python
python plot.py -i 1.png -c curve.h5 -s curve_new.h5 -y 1024　
'''
img_path:　图片路径
curve_path:　曲线文件路径(*.h5)
curve_save_path: 新曲线储存文件路径(*.h5)
vert_num: 曲线上初始点个数, 点太密集不好调试
y_length: yuv中ｙ的范围，默认10bit 1024, 可选择8bit 256
'''
```





