"""
===========
Path Editor
===========

Sharing events across GUIs.

This example demonstrates a cross-GUI application using Matplotlib event
handling to interact with and modify objects on the canvas.
"""

import numpy as np
from scipy.interpolate import interp1d
from matplotlib.backend_bases import MouseButton
from matplotlib.lines import Line2D
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import matplotlib.pyplot as plt
import h5py
from PIL import Image
import argparse
global_res_y = None


'''
查看图片
'''
class ImagePlot:
    def __init__(self, ax, img_path, limit_range):
        assert img_path, 'Image path is not available'
        self.ax = ax
        self.y_val_range = limit_range['X'][1] - limit_range['X'][0] + 1
        self.prepoint = self.preanno = None
        self.im = Image.open(img_path)
        ax.imshow(self.im)
        ax.format_coord = self.format_coord 
        ax.set_title(
            'Info is show in the navigation bar\nor Double click to mark')

        # event listen
        on_move_id = self.ax.figure.canvas.mpl_connect('button_press_event', self.on_move)
    @staticmethod 
    def get_Coef(y_val):
        global global_res_y
        return float(format(global_res_y[y_val],'.4f'))
    def getY(self, R, G, B):
        # return (0.299*R[0] + 0.587*G[0] + 0.114*B[0]) * 4 # 256->1024
        res = np.dot([R[0], G[0], B[0]], [65.481, 128.553, 24.966]) / 255.0 + 16.0
        if self.y_val_range == 1024:
            return int(res*4)
        return int(res)
    # 定义鼠标响应函数
    def on_move(self, event):
        if not event.dblclick:
            return
        global im, prepoint, preanno
        # 标注点的坐标：
        point_x = event.xdata
        point_y = event.ydata
        # print(im.getpixel((point_x, point_y)))

        point, = self.ax.plot(point_x, point_y, '.', c='firebrick')
        # 标注框偏移量
        offset1 = 10
        offset2 = 10
        # 标注框
        bbox2 = dict(boxstyle="round", fc='salmon', alpha=0.6)
        # 标注箭头
        arrowprops2 = dict(arrowstyle="->", connectionstyle="arc3,rad=0.")
        # 标注
        RGB = self.im.getpixel((point_x, point_y))[:3]
        y_val = self.getY(*zip(RGB))
        annotation = self.ax.annotate('RGB:{}\n Y:{} Strength:{}'.format(RGB, y_val, self.get_Coef(y_val)), xy=(point_x, point_y), xytext=(-offset1, offset2), textcoords='offset points',
                                bbox=bbox2, arrowprops=arrowprops2, size=10)
        # 默认鼠标未指向时不显示标注信息
        annotation.set_visible(True)
        plt.draw()
        # self.ax.canvas.draw()

        if self.prepoint is not None:
            self.prepoint.remove()
        self.prepoint = point
        if self.preanno is not None:
            self.preanno.remove()
        self.preanno = annotation    


    def format_coord(self, x, y):
        y_val = self.getY(*zip(self.im.getpixel((x, y))[:3]))
        return 'Coord[x=%.1f, y=%.1f]\n Y in YUV=%d, Strength=%.4f' % (x, y, y_val, self.get_Coef(y_val)) 


'''
折线图
'''


class CurvePlot:
    """
    An path editor.

    Press 't' to toggle vertex markers on and off.  When vertex markers are on,
    they can be dragged with the mouse.
    """

    showverts = True
    epsilon = 10  # max pixel distance to count as a vertex hit

    def __init__(self, ax, verts, limit_range):
        # 曲线
        x, y = self.interpolate(*zip(*verts), limit_range)
        spline = Line2D(x, y)
        ax.add_line(spline)
        ax.set_title(
            'Drag points to update path, \nButton "I"[insert point], "H"[hide line], "D"[delete point(>3)]')
        ax.set_xlim(0, (limit_range['X'][1]//100+1) * 100)
        ax.set_ylim(0, 1.1)
        ax.set_xlabel('Value Of Y')
        ax.set_ylabel('Denoise Strength')

        self.ax = spline.axes  # pathpatch.axes
        canvas = self.ax.figure.canvas
        self.verts = verts
        self.spline = spline
        self.spline.set_animated(True)
        self.preanno = None
        # 直线
        x, y = zip(*self.verts)
        self.line, = ax.plot(x, y, marker='o', ls='--',
                             markerfacecolor='r', animated=True)

        self._ind = None  # the active vertex

        canvas.mpl_connect('draw_event', self.on_draw)
        canvas.mpl_connect('button_press_event', self.on_button_press)
        canvas.mpl_connect('key_press_event', self.on_key_press)
        canvas.mpl_connect('button_release_event', self.on_button_release)
        canvas.mpl_connect('motion_notify_event', self.on_mouse_move)
        self.canvas = canvas

        # limit of point and curve
        limit_plot = [
            # (limit_range['X'][0], limit_range['Y'][0]),
            (limit_range['X'][0], limit_range['Y'][1]),
            (limit_range['X'][1], limit_range['Y'][1]),
            (limit_range['X'][1], limit_range['Y'][0])
        ]
        x_limit, y_limit = zip(*limit_plot)
        self.limit_plot, = ax.plot(x_limit, y_limit, ls='--', lw=1,
                                   markerfacecolor='b', animated=True)
        self.limit_range = limit_range
    
    @staticmethod
    def interpolate(x, y, limit_range):
        res_x = np.arange(limit_range['X'][0], limit_range['X'][1]+1, 1)
        res_y = interp1d(x, y, kind='cubic')(res_x)
        res_x = [max(limit_range['X'][0], min(i, limit_range['X'][1]))
                for i in res_x]
        res_y = [max(limit_range['Y'][0], min(i, limit_range['Y'][1]))
                for i in res_y]
        global global_res_y
        global_res_y = res_y
        return res_x, res_y

    @staticmethod
    def dist(x, y):
        """
        Return the distance between two points.
        """
        d = x - y
        return np.sqrt(np.dot(d, d))

    def dist_point_to_segment(self, p, s0, s1):
        """
        Get the distance of a point to a segment.
        *p*, *s0*, *s1* are *xy* sequences
        This algorithm from
        http://geomalgorithms.com/a02-_lines.html
        """
        v = s1 - s0
        w = p - s0
        c1 = np.dot(w, v)
        if c1 <= 0:
            return self.dist(p, s0)
        c2 = np.dot(v, v)
        if c2 <= c1:
            return self.dist(p, s1)
        b = c1 / c2
        pb = s0 + b * v
        return self.dist(p, pb)

    def get_ind_under_point(self, event):
        """
        Return the index of the point closest to the event position or *None*
        if no point is within ``self.epsilon`` to the event position.
        """
        # display coords
        # xy = verts #np.asarray(self.pathpatch.get_path().vertices)
        xyt = self.spline.get_transform().transform(self.verts)
        xt, yt = xyt[:, 0], xyt[:, 1]
        d = np.sqrt((xt - event.x)**2 + (yt - event.y)**2)
        ind = d.argmin()

        if d[ind] >= self.epsilon:
            ind = None

        return ind

    def on_draw(self, event):
        """Callback for draws."""
        self.background = self.canvas.copy_from_bbox(self.ax.bbox)
        self.ax.draw_artist(self.spline)
        self.ax.draw_artist(self.line)
        self.ax.draw_artist(self.limit_plot)
        self.canvas.blit(self.ax.bbox)

    def on_button_press(self, event):
        """Callback for mouse button presses."""
        if (event.inaxes is None
                or event.button != MouseButton.LEFT
                or not self.showverts):
            return
        self._ind = self.get_ind_under_point(event)

    def on_button_release(self, event):
        """Callback for mouse button releases."""
        if (event.button != MouseButton.LEFT
                or not self.showverts):
            return
        self._ind = None
        if self.preanno is not None:
            self.preanno.remove()
            self.preanno = None
            self.canvas.draw()
    def on_key_press(self, event):
        """Callback for key presses."""
        if not event.inaxes:
            return
        if event.key == 'h':
            self.showverts = not self.showverts
            self.line.set_visible(self.showverts)
            if not self.showverts:
                self._ind = None
        elif event.key == 'i':
            xys = self.spline.get_transform().transform(verts)
            p = event.x, event.y
            if self.x_invalid(event.xdata):
                return
            for i in range(len(xys) - 1):
                s0 = xys[i]
                s1 = xys[i + 1]
                d = self.dist_point_to_segment(p, s0, s1)
                if d <= self.epsilon:
                    self.verts.insert(i+1, (event.xdata, event.ydata))
                    self.line.set_data(zip(*self.verts))
                    x, y = self.interpolate(*zip(*self.verts), self.limit_range)
                    self.spline.set_data(x, y)
        elif event.key == 'd':
            p = event.x, event.y
            self._ind = self.get_ind_under_point(event)
            if self._ind is not None and len(self.verts) > 4:
                self.verts.pop(self._ind)
                self.line.set_data(zip(*self.verts))
                x, y = self.interpolate(*zip(*self.verts), self.limit_range)
                self.spline.set_data(x, y)
        self.canvas.draw()
    def x_invalid(self, x):
        # ｘ坐标不能重叠，否则插值失败
        return x in [item[0] for item in list(self.verts)]       
    def on_mouse_move(self, event):
        """Callback for mouse movements."""
        if (self._ind is None
                or event.inaxes is None
                or event.button != MouseButton.LEFT
                or not self.showverts):
            return
        # 边界限制
        old_x, old_y = self.verts[self._ind]
        new_x = max(self.limit_range['X'][0], min(
            event.xdata, self.limit_range['X'][1]))
        new_y = max(self.limit_range['Y'][0], min(
            event.ydata, self.limit_range['Y'][1]))
        if self.x_invalid(new_x):
            return
        # 标注框
        bbox2 = dict(boxstyle="round", fc='salmon', alpha=0.6)
        annotation = self.ax.annotate('Y:{%.1f} Strength:{%.4f}'%(new_x, new_y), xy=(new_x, new_y), xytext=(-20, -20), textcoords='offset points',
                                bbox=dict(boxstyle="round", fc='salmon', alpha=0.1), size=10)
        annotation.set_visible(True)
        if self.preanno is not None:
            self.preanno.remove()
        self.preanno = annotation    
        self.canvas.draw()

        if (old_x != self.limit_range['X'][0] or old_y != self.limit_range['Y'][0])\
                and (old_x != self.limit_range['X'][1] or old_y != self.limit_range['Y'][1]):
            self.verts[self._ind] = new_x, new_y
            self.line.set_data(zip(*self.verts))
            x, y = self.interpolate(*zip(*self.verts), self.limit_range)

            self.spline.set_data(x, y)
            self.canvas.restore_region(self.background)
            self.ax.draw_artist(self.spline)
            self.ax.draw_artist(self.line)
            self.ax.draw_artist(self.limit_plot)
            self.canvas.blit(self.ax.bbox)

'''
img_path:　图片路径
curve_path:　曲线文件路径(*.h5)
curve_save_path: 新曲线储存文件路径(*.h5)
vert_num: 曲线上初始点个数, 点太密集不好调试
y_length: yuv中ｙ的范围，默认10bit 1024, 可选择8bit 256
'''
def work(img_path, curve_path, curve_save_path, vert_num, y_length):
    # 曲线选择范围, 默认[0, 1023]
    limit_range = {'X': [0, y_length-1], 'Y': [0, 1]}
    # 读取初始坐标
    global verts
    assert curve_path, 'curve file is not exists'
    f = h5py.File(curve_path, 'r')
    verts_initial = list(f['coord'])
    f.close()
    verts = []
    # 稀疏处理, 同时需要选择0, 1023对应点
    verts_initial_len = len(verts_initial)
    stride = (verts_initial_len-1) // (max(1, vert_num-2))
    for i in range(0, verts_initial_len-1, stride):
        verts.append(verts_initial[i])
    verts.append(verts_initial[-1])

    # 显示图像
    fig, axes = plt.subplots(1,2)
    image_plot = ImagePlot(axes[0], img_path, limit_range)
    interactor = CurvePlot(axes[1], verts, limit_range)
    plt.show()

    # curve 存储
    res_x, res_y = interactor.interpolate(*zip(*verts), limit_range)
    res_y = [float(format(num, '.4f')) for num in res_y]
    corrd = []
    for x, y in zip(res_x, res_y):
        corrd.append((x, y))
    h = h5py.File(curve_save_path, 'w')
    h.create_dataset("y", data=res_y)
    h.create_dataset("coord", data=corrd)
    h.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--img_path', type=str, required=True, help='Path to origin image.')
    parser.add_argument('-c', '--curve_path', type=str, required=True, help='Path to origin curve.')
    parser.add_argument('-s', '--curve_save_path', type=str, required=True, help='Save path to changed curve.')
    parser.add_argument('-n', '--vert_num', type=int, required=False, default=10, help='Point Number in curve.')
    parser.add_argument('-y', '--y_length', type=int, required=False, default=1024, help='Y length of image.')
    args = parser.parse_args()
    work(args.img_path, args.curve_path, args.curve_save_path, args.vert_num, args.y_length)